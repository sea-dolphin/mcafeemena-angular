import { Component, Input, Output, EventEmitter } from '@angular/core';

declare var jquery:any;
declare var $ :any;
       
@Component({
    selector: 'child-modal-comp',
    templateUrl: '../../templates/modals/modal-info.html'
})
export class ChildModalComponent { 
     
    _showModal: boolean;
    _titleModal: string;
    _contentModal: string;
    
    @Input()
    set showModal(statusModal: boolean) {
        if (statusModal)
        {
            this.openModal();
            this._showModal = true;
        }
    }
    get showModal() {
        return this._showModal;
    }
    
    @Input()
    set childTitleModal(titleModal: string) {
        this._titleModal = titleModal;
    }
    get childTitileModal() {
        return this._titleModal;
    }
    
    @Input()
    set childContentModal(contentModal: string) {
        this._contentModal = contentModal;
    }
    get childContentModal() {
        return this._contentModal;
    }
    
    openModal() {
        $("#myModal").modal('show');
    }
    
}