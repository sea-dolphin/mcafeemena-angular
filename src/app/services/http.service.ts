
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Response, Headers, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
 
@Injectable()
export class HttpService {
 
    constructor(private http: Http) { }
     
    requestGet() {
        return this.http.get('http://mcafeemena.dkonovalov.lara7/api/infomedia/sendPhone')
    }
    
    postData(url: string, data: string[]) {
        
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' }); 
        var params = new URLSearchParams();
        
        for (let key in data) {
            params.set(key, data[key]);
        }
        
        return this.http.post(url, params.toString(), { headers: headers })
                        .map(res => res.json())
                        .catch((error:any) => {return Observable.throw(error);});        
    }
}


