import { BrowserModule }        from '@angular/platform-browser';
import { NgModule }             from '@angular/core';
import { HttpModule }           from '@angular/http';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule }          from '@angular/forms';
//import { ModalModule }          from 'ngx-bootstrap/modal';

import { AppComponent } from './app.component';

//Test pages
import { AboutComponent } from './pages/about.component';
import { HomeComponent }   from './pages/home.component';
import { NotFoundComponent } from './pages/not-found.component';

//Pages
import { BhFlow1Component } from './pages/bh/bh-flow1.component';
import { BhGetPhoneComponent } from './pages/bh/bh-get-phone.component';

//Child component
import { ChildModalComponent } from './components/modals/child-modal.component';

//Translate
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

// Definition of Routes
const appRoutes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'about', component: AboutComponent },
    
    //BH
    { path: 'bh/flow1', component: BhFlow1Component },
    { path: 'bh/getPhone', component: BhGetPhoneComponent },
    
    //Not found
    { path: '**', component: NotFoundComponent },
];

export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http);
}

@NgModule({
    declarations: [
        AppComponent, HomeComponent, AboutComponent, NotFoundComponent,
        //BH
        BhFlow1Component, BhGetPhoneComponent,
        //Child component
        ChildModalComponent
    ],
    imports: [
        BrowserModule, HttpModule, RouterModule.forRoot(appRoutes), FormsModule,  /*ModalModule.forRoot(),*/
        HttpClientModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        })
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
