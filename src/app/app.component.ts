import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})

export class AppComponent {
    
    currentLangEn: boolean = true;
    
    constructor(private translate: TranslateService) {
        
        translate.setDefaultLang('en');
    }
    
    switchLanguage(language: string) {
        this.translate.use(language);
        this.currentLangEn = !this.currentLangEn;
    }
}