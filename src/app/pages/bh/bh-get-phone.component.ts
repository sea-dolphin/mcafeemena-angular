
import { Component, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

import { HttpService } from '../../services/http.service';

import { ChildModalComponent } from '../../components/modals/child-modal.component';

declare var jquery: any;
declare var $: any;
declare var LogCTA: any;

@Component({
    selector: 'bh-get-phone-app',
    templateUrl: '../../templates/bh/default.html',
    providers: [ HttpService ]
})

export class BhGetPhoneComponent implements OnDestroy {
     
    private phone: string;
    private mcc: string;
    private mnc: string;
    data: string[] = new Array();
    result: string;
    showElement: boolean = true;
    
    //AJAX url
    ajaxUrl: string;
    
    //dynamic pages
    pageEnterPhone: boolean = true;
    pageGetPin: boolean = false;
    pageEnterEmail: boolean = false;
    pageSuccess: boolean = false;
    pageSubscribe: boolean = false;
    pageConfirmSubscribe: boolean = false;
    
    //Child variables
    statusModal: boolean = false;
    titleModal: string;
    contentModal: string;
    
    //access to child = console.log(this.chil)
    @ViewChild(ChildModalComponent) child; 
    
    private routeSubscription: Subscription;
    private querySubscription: Subscription;
    
    constructor(private route: ActivatedRoute, private httpService: HttpService) {
        
        this.routeSubscription = route.params.subscribe(
            params => this.phone = params['mx']
        );
        
        this.querySubscription = route.queryParams.subscribe(
            (queryParam: any) => {
                this.phone = queryParam['mx'];
                this.mcc =   queryParam['mcc'];
                this.mnc =   queryParam['mnc'];
            }
        );
        
        if (this.phone === '') {
            // return
        }
        else {
            this.pageEnterPhone = false;
            this.pageSubscribe = true;
        }
    }
    
    ngOnInit() {
        //for local develop
        if ( window.location.hostname == 'localhost' ) {
            this.ajaxUrl = 'http://mcafeemena.dkonovalov.lara7/api/';
        }
        else {
            this.ajaxUrl = 'http://api.mcafeemena.com/api/';
        }
    }
    
    myTest() {
        //$('[data-toggle=model]').modal();
        $("#myModal").modal('show');
    }
    
    testChild() {
        this.titleModal = 'Title model';
        this.contentModal = 'Content modal'
        this.statusModal = !this.statusModal;
        
        //console.log(this.statusModal);
    }
    
    openModal(title: string, content: string) {
        this.titleModal = title;
        this.contentModal = content;
        this.statusModal = true;
    }
    
    ngAfterViewInit() {
        //console.log('only after THIS EVENT "child" is usable!!', this.child);
    }
    
    sendPhone(enterPhoneNumber: string) {
        
        //this.httpService.requestGet().subscribe((data: Response) => this.result=data.json());;
        this.showElement = false;
        
        this.data['phone'] = enterPhoneNumber;
        this.httpService.postData(this.ajaxUrl + 'infomedia/sendPhone', this.data)
                .subscribe((data_result) => {
                    this.result = data_result; 
                    this.showPinPage(this.result);
                });
    }
    
    showPinPage(result) {
        console.log(result.success);
        if ( result.success )
        {
            this.showElement = true;
            this.pageGetPin = true;
            this.pageEnterPhone = false;
        }        
    }
    
    checkPinCode(enterPinCode: string) {
        this.showElement = false;
        
        this.data['pin'] = enterPinCode;
        this.httpService.postData(this.ajaxUrl + 'infomedia/checkPin', this.data)
                .subscribe((data_result) => {
                    this.result = data_result; 
                    this.checkResponsePinCode(this.result);
                });
    }
    
    checkResponsePinCode(result) {
        if ( result.success )
        {
            this.getInfomediaData();
            this.sendAllDataToInfomedia();
        }
        else
        {
            this.openModal('Error', 'Pin code incorrect');
            this.showElement = true;
        }
    }
    
    sendAllDataToInfomedia() {
        
        this.httpService.postData(this.ajaxUrl + 'infomedia/sendAllDataToInfomedia', this.data)
                .subscribe((data_result) => {
                    this.result = data_result; 
                    //this.resultSubscribe(this.result);
                    this.resultSendAllData(this.result);
                });
    }
    
    getInfomediaData() {
        
        this.httpService.postData(this.ajaxUrl + 'infomedia/getInfomediaData', [])
                .subscribe((data_result) => {
                    this.result = data_result;
                    this.infimediaScreenshot(this.result);
                });
    }
    
    infimediaScreenshot(result) {
        LogCTA(result.IM_PI, '', '', '', result.IM_PF + '_First', result.PGW_COMPANY, result.MD5Hash);
    }
    
    resultSendAllData(result: any) {
        if ( result.success )
        {
            this.pageGetPin = false;
            this.pageEnterEmail = true;
            this.showElement = true;
        }
        else
        {
            this.openModal('Error', 'Error subscribe');
            this.showElement = true;
        }
    }
    
    createLicense(enterEmail: string) {
        if ( enterEmail ) {
            this.showElement = false;
            this.data['emailAddress'] = enterEmail;
            
            this.httpService.postData(this.ajaxUrl + 'infomedia/createLicense', this.data)
                .subscribe((data_result) => {
                    this.result = data_result;
                    //this.responseCreateLicense(this.result);
                    this.resultSubscribe(this.result);
                });
        }
        else {
            this.openModal('Error', 'Email incorrect');
        }
    }
    
//    responseCreateLicense(result: any) {
//        if ( result.success )
//        {
//            this.pageSuccess = true;
//            this.pageEnterEmail = false;
//        }
//        else
//        {
//            this.openModal('Error', 'Error license');
//            this.showElement = true;
//        }
//    }
    
    resultSubscribe(result) {
        if ( result.success )
        {
            this.pageSuccess = true;
            //this.pageGetPin = false;
            //this.pageEnterPhone = false;
            this.pageEnterEmail = false;
        }
        else
        {
            this.openModal('Error', 'Error license');
            this.showElement = true;
        }
    }
    
    /*
     * IF Infomedia sender phone number
     */
    buttonSubscribe() {
        this.pageSubscribe = false;
        this.pageConfirmSubscribe = true;
        
        this.data['mx'] = this.data['phone'];
        this.data['mcc'] = this.mcc;
        this.data['mnc'] = this.mnc;
        
        this.getInfomediaData();
    }
    
    buttonConfirmSubscribe() {
        this.getInfomediaData();
        this.sendAllDataToInfomedia();
    }
    
    ngOnDestroy() {
        this.routeSubscription.unsubscribe();
        this.querySubscription.unsubscribe();
    }
}