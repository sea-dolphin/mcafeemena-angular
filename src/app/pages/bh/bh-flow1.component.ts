
import { Component } from '@angular/core';

@Component({
    selector: 'bh-flow1-app',
    templateUrl: '../../templates/bh/loading.html'
})

export class BhFlow1Component {
     
    ngOnInit() {
        if ( window.location.hostname == 'localhost' ) {
            location.href = "http://wbg.infomediatechnologies.com/iablu/V/1/6/?m=redirect&ru=http://localhost:4200/bh/getPhone";
        }
        else {
            location.href = "http://wbg.infomediatechnologies.com/iablu/V/1/6/?aid=177&pid=92000180&m=redirect&ru=http://bh.mcafeemena.com/bh/getPhone";
        }
    }
}